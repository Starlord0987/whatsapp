import React, { useContext, useEffect, useState } from "react";
import { plane, refresh } from "../assets/icons/AllIcon";

import GlobalContext from "../Context/GlobalContext";
import { useNavigate } from "react-router-dom";

const AvatarList = () => {
  const {
    users,
    setNumber,
    getTargetedUsers,
    setCurrNum,
    dropData
  } = useContext(GlobalContext);
  

  const navigate = useNavigate();

  const handleLogout = () => {
    // alert('navigate to')
    localStorage.removeItem("loginToken");
    navigate("/login");
  };



  const img = "https://cdn-icons-png.flaticon.com/512/149/149071.png";
  // console.log("uuu", users);
  // console.log("crr", dropData);
  const handleChangeUser = (e) => {
    setCurrNum(e.target.value);
    // alert(currNum + e.target.value);
  };
  return (
    <>
      <div className="py-5 px-5 font-extrabold text-lg flex justify-between">
        <div className="mt-1">
          <button
            onClick={() => handleLogout()}
            type="button"
            className="group relative flex w-full justify-center rounded-lg px-2 py-1.5 text-sm text-red-500 hover:bg-gray-50 hover:text-gray-700"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className=" w-5 opacity-75"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth="2"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
              />
            </svg>

            <span className="absolute left-full top-1/2 ml-4 -translate-y-1/2 rounded bg-gray-900 px-2 py-1.5 text-xs font-medium text-white opacity-0 group-hover:opacity-100">
              Logout
            </span>
          </button>
        </div>
        <div className="flex ">
          <div className="xl:w-88 px-1">
            <select
              onChange={(e) => handleChangeUser(e)}
              className="form-select appearance-none
      block
      w-full
      px-10
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              aria-label="Default select example"
            >
              {dropData.map((item, id) => (
                <option key={id} value={item.mobile_number}>
                  {item.mobile_number}
                </option>
              ))}
            </select>
          </div>
          <div
            className="cursor-pointer rotate-90 mr-3"
            onClick={() => getTargetedUsers()}
          >
            {" "}
            {refresh}
          </div>
        </div>
      </div>

      <div className=" px-5 bg-grey-lightest py-3">
        <input
          type="text"
          className="w-full px-2 py-4 text-sm rounded-lg bg-[#f2f2f2]"
          placeholder="Search or start new chat"
        />
      </div>

      <div className="bg-grey-lighter flex-1 py-2 overflow-auto">
        {users.map((item, id) => (
          <div
            key={id}
            onClick={() => setNumber(item.sender_no)}
            className="px-3 py-2 flex items-center bg-grey-light cursor-pointer"
          >
            <div>
              <img className="h-12 w-12 rounded-full" src={img} />
            </div>
            <div className="ml-4 flex-1 border-b border-grey-lighter py-4">
              <div className="flex items-bottom justify-between">
                <p className="text-grey-darkest">{item.sender_no}</p>

                <p className="text-xs text-grey-darkest">
                  {" "}
                  {item.sender_name}{" "}
                </p>
              </div>
              {/* <p className="text-grey-dark mt-1 text-sm">{item.chat}</p> */}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default AvatarList;
