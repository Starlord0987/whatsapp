import React, { useContext, useState } from "react";
import { useEffect } from "react";
import AvatarList from "./Components/AvatarList";
import Chat from "./Components/Chat";
import GlobalContext from "./Context/GlobalContext";
import { useNavigate } from "react-router-dom";
import { Backdrop } from "@mui/material";
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';

function Home() {
  const [show, setShow] = useState(0);

  const { setNumber, users, chatData, open, handleClose} = useContext(GlobalContext);
  const indexTab = (index) => {
    console.log("d", index);
    setShow(index);
  };

  // console.log("szfdzf", number);

  const isAuthenticated = localStorage.getItem("loginToken");
  const navigate = useNavigate();
  useEffect(() => {
    if (isAuthenticated === null) {
      navigate("/login");
      console.log("enterd in not authenticated");
    } else {
      console.log("entered in authenticated");
    }
  }, [isAuthenticated]);

  return (
    <>
    <Backdrop
  sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
  open={open}
  onClick={handleClose}
>
  <CircularProgress color="inherit" />
</Backdrop>
      <div className=" h-screen">
        <div className="flex border border-grey rounded shadow-lg h-full">
          <div className="mb-[10vh] md:mb-0  w-full lg:w-1/3 md:w-1/3 border flex flex-col ">
            {show === 0 ? (
              <AvatarList  setNumber={setNumber} users={users} />
            ) : (
              ""
            )}
            {/* {show === 1 ? <Group /> : ""}
            {show === 2 ? <Setting /> : ""} */}
          </div>

          <div className=" leading-none lg:w-2/3 md:w-2/3 border  hidden md:flex flex-col">
            <Chat chatData={chatData} />
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
