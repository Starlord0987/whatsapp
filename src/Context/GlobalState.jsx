import React, { useState, useEffect } from "react";
import { API_URL } from "../Components/Constant/api";
import { getRequest } from "../Components/Constant/ApiCall";
import GlobalContext from "./GlobalContext";

const GlobalState = (props) => {
  const [users, setUsers] = useState([]);
  const [number, setNumber] = useState();
  const [chatData, setChatData] = useState([]);
  const [userNum, setUserNum] = useState(0);
  const [currNum, setCurrNum] = useState();
  const [dropData, setDropData] = useState([]);
  const [open, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  const user_id = localStorage.getItem("user_id");

  const getCurrAccData = async () => {
    try {
      handleToggle();
      const res = await getRequest(`/user_access/${user_id}`);
      const resData = await res.json();
      // console.log("resdefault", resData);
      setDropData(resData);
      getTargetedUsers(resData[0]["mobile_number"]);
    } catch (err) {
      console.log("err", err);
    }
  };

  const getTargetedUsers = async (curr_num) => {
    try {
      const res = await fetch(
        `${API_URL}/wp_users/${currNum ? currNum : curr_num}`
      );
      const resData = await res.json();
      // console.log("targetted_user_number", resData);
      getChatData(curr_num, resData[0]["sender_no"]);
      setUsers(resData);
      handleClose();
    } catch (error) {
      console.log("err", error);
    }
  };

  const getChatData = async (curr_num, sender_no) => {
    try {
      const res = await fetch(
        `${API_URL}/chat_data/${number ? number : sender_no}/${
          currNum ? currNum : curr_num
        }`
      );
      // console.log("chat_data_res", res);
      const resData = await res.json();
      setChatData(resData.chat_data);
    } catch (error) {
      console.log("err", error);
    }
  };

  useEffect(() => {
    getCurrAccData();
  }, [currNum, number]);

  return (
    <>
      <GlobalContext.Provider
        value={{
          users: users,
          setNumber: setNumber,
          number: number,
          chatData: chatData,
          userNum: userNum,
          getChatData: getChatData,
          getTargetedUsers: getTargetedUsers,
          getCurrAccData: getCurrAccData,
          setCurrNum: setCurrNum,
          currNum: currNum,
          dropData: dropData,
          handleClose: handleClose,
          open: open,
        }}
      >
        {props.children}
      </GlobalContext.Provider>
    </>
  );
};

export default GlobalState;
